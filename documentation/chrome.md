# Chrome Tab for External URLs

Configure Chrome tab to render external links with chrome engine within your app, instead of using native webview or web browser.

## Setting up firebase
* First enable `CONFIG_EXTURL` to open URL externally instead of native webview
* Now set `CONFIG_TAB` to `true` to let links to be handled by default chrome browser engine
