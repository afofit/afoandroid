package com.afo.fit;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;

import java.util.HashMap;
import java.util.Map;

public class FirestoreDB {

	private FirebaseFirestore db;
	public FirestoreDB() {
		db = FirebaseFirestore.getInstance();
	}

	//Update FCM token to firestore DB
	public void updateFCMToken(final String userId,final String fcmToken) {
		Map<String, String> map = new HashMap<>();
		map.put("FcmToken", fcmToken);


	db.collection("users")
		.document(userId)
		.set(map, SetOptions.merge()).
			addOnSuccessListener(new OnSuccessListener<Void>() {
				@Override
				public void onSuccess(Void aVoid) {
					// on successful completion of this process
					System.out.println("Success");
		                    }
		                }).addOnFailureListener(new OnFailureListener() {
		            // inside on failure method we are
		            // displaying a failure message.
		            @Override
		            public void onFailure(@NonNull Exception e) {
						System.out.println("Failure" );
		            }
		 });

	}
}
