package com.afo.fit;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.afo.fit.models.PathParam;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;

import static com.afo.fit.constants.AfoConstants.PATH_PARAM_KEY;

public class SplashScreen extends Activity {

	private String eventId = null;
	private String referredBy = null;
	private String code = null;
	private static int SPLASH_TIME_OUT = 6500;
	private PrefManager prefManager;
	private boolean isFirstTime = false;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

		prefManager = new PrefManager(this);

		isFirstTime = prefManager.isFirstTimeLaunch();

		FirebaseDynamicLinks.getInstance().getDynamicLink(getIntent()).addOnSuccessListener(
				this, new OnSuccessListener<PendingDynamicLinkData>() {
					@Override
					public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
						Log.i("SplashScreen", "We have a dynamic link ");

						Uri deepLink = null;
						if(pendingDynamicLinkData != null) {
							deepLink = pendingDynamicLinkData.getLink();
						}

						if(deepLink!= null) {
							Log.i("SplashScreen", "Here's the deep link URL:\n"+
									deepLink.toString());
							eventId = deepLink.getQueryParameter("eventId");
							code = deepLink.getQueryParameter("code");
							referredBy = deepLink.getQueryParameter("referredBy");

							Log.i("SplashScreen", "OnSuccess: eventID found "+ eventId);
						}
						if (!isFirstTime) {
							Log.i("SplashScreen", "EventID:\n"+
									eventId);
							Intent intent = new Intent(SplashScreen.this, MainActivity.class);

							final PathParam pathParam = new PathParam(eventId,  code, referredBy, false,null);
							intent.putExtra(PATH_PARAM_KEY, pathParam);
							startActivity(intent);
							finish();
						} else {

							ImageView imageView = findViewById(R.id.splashImage);

							Glide.with(SplashScreen.this)
									.asGif()
									.load(R.raw.splash_logo1)
									.listener(new RequestListener<GifDrawable>() {
										@Override
										public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<GifDrawable> target, boolean isFirstResource) {
											return false;
										}

										@Override
										public boolean onResourceReady(GifDrawable resource, Object model, Target<GifDrawable> target, DataSource dataSource, boolean isFirstResource) {
											resource.setLoopCount(1);

											new Handler().postDelayed(new Runnable() {

												@Override
												public void run() {
													// This method will be executed once the timer is over
													// Start your app main activity

													prefManager.setFirstTimeLaunch(false);
													final PathParam pathParam = new PathParam(eventId ,code, referredBy, true,null);
													Intent i = new Intent(SplashScreen.this, WelcomeActivity.class);
													i.putExtra(PATH_PARAM_KEY, pathParam);
													startActivity(i);

													// close this activity
													finish();
												}
											}, SPLASH_TIME_OUT);
											return false;
										}
									}).into(imageView);
						}
					}

				}
		).addOnFailureListener(this, new OnFailureListener() {
			@Override
			public void onFailure(@NonNull Exception e) {
				Log.i("SplashScreen", "Error in getting dynamic link", e);
			}
		});
    }
}
