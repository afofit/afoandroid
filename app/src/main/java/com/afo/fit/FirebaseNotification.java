package com.afo.fit;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class FirebaseNotification extends FirebaseMessagingService {

	private FirestoreDB firestoreDB;
	private PrefManager prefManager;
	Context context = this;
	@Override
	public void onNewToken(String s) {
		Log.d("TOKEN_REFRESHED ", s);

		// printing new tokens in logcat
		firestoreDB = new FirestoreDB();
		prefManager = new PrefManager(this);
		firestoreDB.updateFCMToken(prefManager.getUserID(),s);
	}

	public void onMessageReceived(RemoteMessage message) {
		if (message.getData() != null) {
           System.out.println("Datatattatatta"+message.getData());
			showNotification(
					message.getData().get("title"),
					message.getData().get("body"),
					message.getData().get("notificationAction"),
					message.getData().get("notificationType"),
					message.getData().get("notificationImage"),
					message.getData().get("notificationBanner"));
//			showNotification(message.getNotification().getTitle(),
//					message.getNotification().getBody(), null,null,null,null);

		}
	}

	// Method to get the custom Design for the display of
	// notification.
	private RemoteViews getCustomDesign(String title,
										String message,
										String notificationType,
										String notificationImage,
										String notificationBanner) {

		System.out.println("Title:"+title+" message : "+message+"notificationType"+notificationType+"Image"+notificationImage+"banner"+notificationBanner);
		RemoteViews remoteViews = new RemoteViews(
				getApplicationContext().getPackageName(),
				R.layout.notification_layout);
		remoteViews.setTextViewText(R.id.title, title);
		remoteViews.setTextViewText(R.id.message, message);
		if(notificationImage!=null)

		{
			if(notificationType!=null&&notificationType.equalsIgnoreCase("message"))
			{
				remoteViews.setImageViewUri(R.id.notificationImage1, Uri.parse(notificationImage));
			}else {
				remoteViews.setImageViewUri(R.id.notificationImage2, Uri.parse(notificationImage));
			}
		}
		if(notificationBanner!=null)
		{
			remoteViews.setImageViewUri(R.id.notificationBanner, Uri.parse(notificationBanner));
		}

		return remoteViews;
	}

	// Method to display the notifications
	public void showNotification(String title,
								 String message,
								 String notificationAction,
								 String notificationType,
								 String notificationImage,
								 String notificationBanner) {
		// Pass the intent to switch to the MainActivity
		Intent intent
				= new Intent(this, MainActivity.class);
		intent.putExtra("notificationAction", notificationAction);


		// Assign channel ID
		String channel_id = "notification_channel";
		// Here FLAG_ACTIVITY_CLEAR_TOP flag is set to clear
		// the activities present in the activity stack,
		// on the top of the Activity that is to be launched
		intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
		// Pass the intent to PendingIntent to start the
		// next Activity
		PendingIntent pendingIntent
				= PendingIntent.getActivity(
				this, 0, intent,
				PendingIntent.FLAG_ONE_SHOT);
		Uri soundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

		// Create a Builder object using NotificationCompat
		// class. This will allow control over all the flags
		NotificationCompat.Builder builder
				= new NotificationCompat
				.Builder(getApplicationContext(),
				channel_id)
				.setSmallIcon(R.mipmap.ic_launcher)
				.setAutoCancel(true)
				.setVibrate(new long[]{1000, 1000, 1000,
						1000, 1000})
				.setOnlyAlertOnce(true)
				.setSound(soundUri)
				.setContentIntent(pendingIntent);

		// A customized design for the notification can be
		// set only for Android versions 4.1 and above. Thus
		// condition for the same is checked here.
		if (Build.VERSION.SDK_INT
				>= Build.VERSION_CODES.JELLY_BEAN) {
			builder = builder.setContent(
					getCustomDesign(title, message,notificationType,notificationImage,notificationBanner));
		} // If Android Version is lower than Jelly Beans,
		// customized layout cannot be used and thus the
		// layout is set as follows
		else {
			builder = builder.setContentTitle(title)
					.setContentText(message)
					.setSound(soundUri)
					.setSmallIcon(R.mipmap.ic_launcher);
		}
		// user of events that happen in the background.
		NotificationManager notificationManager
				= (NotificationManager) getSystemService(
				Context.NOTIFICATION_SERVICE);
		// Check if the Android Version is greater than Oreo
		if (Build.VERSION.SDK_INT
				>= Build.VERSION_CODES.O) {
			NotificationChannel notificationChannel
					= new NotificationChannel(
					channel_id, "web_app",
					NotificationManager.IMPORTANCE_HIGH);
			notificationManager.createNotificationChannel(
					notificationChannel);
		}

		notificationManager.notify(0, builder.build());
	}

	public Bitmap getBitmapfromUrl(String imageUrl) {
		try {
			URL url = new URL(imageUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoInput(true);
			connection.connect();
			InputStream input = connection.getInputStream();
			return BitmapFactory.decodeStream(input);

		} catch (Exception e) {
			Log.e("awesome", "Error in getting notification image: " + e.getLocalizedMessage());
			return null;
		}
	}

	/*private void sendMyNotification(String title, String message, String click_action, String uri, String tag, String nid) {
		//On click of notification it redirect to this Activity
		Intent intent = new Intent(click_action);
		intent.putExtra("uri", uri);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

		int notification_id = nid!=null ? Integer.parseInt(nid) : ASWV_FCM_ID;

		Uri soundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, MainActivity.asw_fcm_channel)
				.setSmallIcon(R.mipmap.ic_launcher)
				.setContentTitle(title+" "+notification_id)
				.setContentText(message)
				.setAutoCancel(true)
				.setSound(soundUri)
				.setContentIntent(pendingIntent);
		Notification noti = notificationBuilder.build();
		noti.flags = Notification.DEFAULT_LIGHTS | Notification.FLAG_AUTO_CANCEL;

		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		notificationManager.notify(notification_id, notificationBuilder.build());
	}*/
}
