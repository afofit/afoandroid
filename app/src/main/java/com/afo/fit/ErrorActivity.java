package com.afo.fit;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import static com.afo.fit.DetectConnection.isInternetAvailable;

public class ErrorActivity extends AppCompatActivity {
	Button retryButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error);
        retryButton = (Button)findViewById(R.id.retryButton);


        retryButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(isInternetAvailable(getApplicationContext())) {
					Intent intent = new Intent(getApplicationContext(), MainActivity.class);
					getApplicationContext().startActivity(intent);
					// close this activity
					//finish();
				}

			}
		});

		retryButton.setOnTouchListener(new View.OnTouchListener() {

			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN: {
						v.getBackground().setColorFilter(0xe0f47521, PorterDuff.Mode.SRC_ATOP);
						v.invalidate();
						break;
					}
					case MotionEvent.ACTION_UP: {
						v.getBackground().clearColorFilter();
						v.invalidate();
						break;
					}
				}
				return false;
			}
		});
    }
}
