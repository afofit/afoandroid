package com.afo.fit;

/*
 * Android Smart WebView is an Open Source Project available on GitHub (https://github.com/mgks/Android-SmartWebView).
 *
 * This program is free to use for private and commercial purposes.
 * Enhance Smart WebView with plugins - https://voinsource.github.io/#plugins (Google Login, Background Services, Vision API, Advance Notifications, PQL etc).
 * Please mention project source or credit developers in your Application's License(s) Wiki.
 * Giving right credit to developers encourages them to create better projects :)
 */

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintJob;
import android.print.PrintManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.URLUtil;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.browser.customtabs.CustomTabsIntent;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.afo.fit.models.PathParam;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.github.drjacky.imagepicker.ImagePicker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.messaging.FirebaseMessaging;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.afo.fit.DetectConnection.isInternetAvailable;
import static com.afo.fit.WebViewConfig.ASWV_SEARCH;
import static com.afo.fit.WebViewConfig.ASWV_SHARE_URL;
import static com.afo.fit.WebViewConfig.CONFIG_CAMUPLOAD;
import static com.afo.fit.WebViewConfig.CONFIG_CERT_VERIFICATION;
import static com.afo.fit.WebViewConfig.CONFIG_CP;
import static com.afo.fit.WebViewConfig.CONFIG_EXC_LIST;
import static com.afo.fit.WebViewConfig.CONFIG_EXTURL;
import static com.afo.fit.WebViewConfig.CONFIG_FUPLOAD;
import static com.afo.fit.WebViewConfig.CONFIG_JSCRIPT;
import static com.afo.fit.WebViewConfig.CONFIG_LOCATION;
import static com.afo.fit.WebViewConfig.CONFIG_OFFLINE;
import static com.afo.fit.WebViewConfig.CONFIG_SFORM;
import static com.afo.fit.WebViewConfig.CONFIG_TAB;
import static com.afo.fit.WebViewConfig.CONFIG_ZOOM;
import static com.afo.fit.WebViewConfig.CUSTOM_USER_AGENT;
import static com.afo.fit.WebViewConfig.OVERRIDE_USER_AGENT;
import static com.afo.fit.WebViewConfig.POSTFIX_USER_AGENT;
import static com.afo.fit.WebViewConfig.USER_AGENT_POSTFIX;
import static com.afo.fit.constants.AfoConstants.PATH_PARAM_KEY;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {


	private static String ASWV_URL = //"https://app.afo.fit/version-test/fitbit_testing?device_type=android";
	WebViewConfig.IS_TEST ?  WebViewConfig.CONFIG_URL_TEST : WebViewConfig.CONFIG_URL;
	private String CURR_URL = ASWV_URL;
	private String eventId = null;
	private String authCode = null;
	private boolean isFirstTime = false;
	private String referredBy = null;
	private String notificationAction = null;

	private String android_id = "";
	WebView mainWebView;

	int asw_error_counter = 0;

	Boolean true_online = !CONFIG_OFFLINE && !ASWV_URL.startsWith("file:///");
	public static String ASWV_HOST = aswm_host(ASWV_URL);

	private String asw_pcam_message, asw_vcam_message;
	private ValueCallback<Uri> asw_file_message;
	private ValueCallback<Uri[]> asw_file_path;
	private final static int asw_file_req = 1;

	private final static int loc_perm = 1;
	private final static int file_perm = 2;

	private PathParam pathParam = null;

	private final SecureRandom random = new SecureRandom();
	private PrefManager prefManager;


	private static final String TAG = MainActivity.class.getSimpleName();
	private Context mContext;
	private WebView mWebviewPop;
	private AlertDialog builder;
	private RelativeLayout relativeLayout;
	private String lastURL = ASWV_URL;
	private FirebaseAuth mAuth;
	private FirestoreDB firestoreDB = new FirestoreDB();


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);

		if (Build.VERSION.SDK_INT >= 21) {
			getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimary));
			Uri[] results = null;
			if (resultCode == Activity.RESULT_CANCELED) {
				if (requestCode == asw_file_req) {
					// If the file request was cancelled (i.e. user exited camera),
					// we must still send a null value in order to ensure that future attempts
					// to pick files will still work.
					asw_file_path.onReceiveValue(null);
					return;
				}
			}
			if (resultCode == Activity.RESULT_OK) {
				results = new Uri[]{intent.getData()};
			}
			asw_file_path.onReceiveValue(results);
			asw_file_path = null;

		} else {
			if (requestCode == asw_file_req) {
				if (null == asw_file_message) return;
				Uri result = intent == null || resultCode != RESULT_OK ? null : intent.getData();
				asw_file_message.onReceiveValue(result);
				asw_file_message = null;
			}
		}
	}

	@SuppressLint({"SetJavaScriptEnabled", "WrongViewCast", "JavascriptInterface"})
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Initialize Firebase Auth
		mAuth = FirebaseAuth.getInstance();

		Intent intent = getIntent();
		pathParam = intent.getParcelableExtra(PATH_PARAM_KEY);
		if (pathParam == null) {
			pathParam = new PathParam(null, null, null, false, null);
		}
		notificationAction = intent.getStringExtra("notificationAction");
		mContext = this.getApplicationContext();
		prefManager = new PrefManager(mContext);

		eventId = pathParam.getEventId();
		referredBy = pathParam.getReferredBy();

		if (intent.getData() != null) {
			final Uri uri = intent.getData();
			authCode = uri.getQueryParameter("code");
			pathParam.setCode(authCode);
		}

		System.out.println("AuthCode: " + authCode);
		isFirstTime = pathParam.isFirstTime();
		Log.i("MainActivity", "EventID:" + eventId);
		setContentView(R.layout.activity_main);
		relativeLayout = findViewById(R.id.splash);
		relativeLayout.setVisibility(View.VISIBLE);

		ImageView loader = (ImageView) findViewById(R.id.loader);
		// showLoaderOrSplash(isFirstTime);
		ImageView splash = (ImageView) findViewById(R.id.splashImage1);
		if (isFirstTime) {
			loader.setVisibility(View.VISIBLE);
			splash.setVisibility(View.GONE);
			//loader
			Glide.with(this)
					.asGif()
					.load(R.raw.loader)
					.into(loader);

		} else {
			loader.setVisibility(View.GONE);
			relativeLayout.setVisibility(View.INVISIBLE);
			splash.setVisibility(View.VISIBLE);
			//Saw splash screen
			Glide.with(this)
					.asGif()
					.load(R.raw.splash_logo1)
					.listener(new RequestListener<GifDrawable>() {
						@Override
						public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<GifDrawable> target, boolean isFirstResource) {
							return false;
						}

						@Override
						public boolean onResourceReady(GifDrawable resource, Object model, Target<GifDrawable> target, DataSource dataSource, boolean isFirstResource) {
							resource.setLoopCount(1);

							return false;
						}
					}).into(splash);
		}

		// prevent app from being started again when it is still alive in the background
		if (!isTaskRoot()) {
			finish();
			return;
		}

		if (!isInternetAvailable(getApplicationContext())) {

			Intent errorIntent = new Intent(MainActivity.this, ErrorActivity.class);
			errorIntent.putExtra(PATH_PARAM_KEY, (Parcelable) pathParam);
			startActivity(errorIntent);
		}


		android_id = Settings.Secure.getString(mContext.getContentResolver(),
				Settings.Secure.ANDROID_ID);

		mainWebView = getMainWebView();


		// swipe refresh

		Handler handler = new Handler();


		//Getting basic device information
		get_info();

		//Getting GPS location of device if given permission
		if (CONFIG_LOCATION && !check_permission(1)) {
			ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, loc_perm);
		}
		//get_location();


		//Reading incoming intents
		readIntents();

		//initializeAdd();

	}

	@Override
	public void onPause() {
		super.onPause();
		mainWebView.onPause();
	}

	@Override
	public void onResume() {
		super.onResume();
		mainWebView.onResume();
		//Coloring the "recent apps" tab header; doing it onResume, as an insurance
		if (Build.VERSION.SDK_INT >= 23) {
			Bitmap bm = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
			ActivityManager.TaskDescription taskDesc;
			taskDesc = new ActivityManager.TaskDescription(getString(R.string.app_name), bm, getColor(R.color.colorPrimary));
			MainActivity.this.setTaskDescription(taskDesc);
		}
		//  get_location();


	}

	//Setting activity layout visibility
	private class Callback extends WebViewClient {
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			//get_location();
		}

		public void onPageFinished(WebView view, String url) {
			relativeLayout.setVisibility(View.GONE);
			mainWebView.setVisibility(View.VISIBLE);

			lastURL = url;
		}

		//For android below API 23
		@Override
		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			//Toast.makeText(getApplicationContext(), getString(R.string.went_wrong), Toast.LENGTH_SHORT).show();
			Intent intent = new Intent(MainActivity.this, ErrorActivity.class);
			startActivity(intent);
			//aswm_view("file:///android_asset/error.html", false, asw_error_counter);
		}

		//Overriding webview URLs
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			CURR_URL = url;
			return false;
		}

		//Overriding webview URLs for API 23+ [suggested by github.com/JakePou]
		@TargetApi(Build.VERSION_CODES.N)
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
			CURR_URL = request.getUrl().toString();
			return url_actions(view, request.getUrl().toString());
		}

		@Override
		public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
			if (CONFIG_CERT_VERIFICATION) {
				super.onReceivedSslError(view, handler, error);
			} else {
				handler.proceed(); // Ignore SSL certificate errors
			}
		}
	}

	//Random ID creation function to help get fresh cache every-time webview reloaded
	public String random_id() {
		return new BigInteger(130, random).toString(32);
	}

	//Opening URLs inside webview with request
	public void aswm_view(String url, Boolean tab, int error_counter) {
		if (error_counter > 2) {
			asw_error_counter = 0;
			aswm_exit();
		} else {
			if (tab || isIntegrationURL(url)) {
				if (CONFIG_TAB) {
					CustomTabsIntent.Builder intentBuilder = new CustomTabsIntent.Builder();
					intentBuilder.setToolbarColor(ContextCompat.getColor(this, R.color.colorPrimary));
					intentBuilder.setSecondaryToolbarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
					intentBuilder.setStartAnimations(this, android.R.anim.slide_in_left, android.R.anim.slide_out_right);
					intentBuilder.setExitAnimations(this, android.R.anim.slide_in_left, android.R.anim.slide_out_right);
					CustomTabsIntent customTabsIntent = intentBuilder.build();
					try {
						customTabsIntent.launchUrl(MainActivity.this, Uri.parse(url));

					} catch (ActivityNotFoundException e) {
						Intent intent = new Intent(Intent.ACTION_VIEW);
						intent.setData(Uri.parse(url));
						startActivity(intent);
					}
				} else {
					Intent intent = new Intent(Intent.ACTION_VIEW);
					intent.setData(Uri.parse(url));
					startActivity(intent);
				}
			} else {
				if (url.contains("?")) { // check to see whether the url already has query parameters and handle appropriately.
					url += "&";
				} else {
					url += "?";
				}
				url += "rid=" + random_id();
				mainWebView.loadUrl(url);
			}
		}
	}

	private boolean isIntegrationURL(String url) {

		return url.startsWith("https://www.fitbit.com/oauth2/")
				|| url.startsWith("https://townscript.com")
				|| url.startsWith("https://www.afsf.in")
				|| url.startsWith("http://www.strava.com/oauth");
	}


	/*--- actions based on URL structure ---*/

	public boolean url_actions(WebView view, String url) {
		boolean a = true;

		// show toast error if not connected to the network
		if (!CONFIG_OFFLINE && !DetectConnection.isInternetAvailable(MainActivity.this)) {
			Toast.makeText(getApplicationContext(), getString(R.string.check_connection), Toast.LENGTH_SHORT).show();

			// use this in a hyperlink to redirect back to default URL :: href="refresh:android"
		} else if (url.startsWith("refresh:")) {
			String ref_sch = (Uri.parse(url).toString()).replace("refresh:", "");
			if (ref_sch.matches("URL")) {
				CURR_URL = ASWV_URL;
			}
			pull_fresh();

			// use this in a hyperlink to launch default phone dialer for specific number :: href="tel:+919876543210"
		} else if (url.startsWith("tel:")) {
			Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse(url));
			startActivity(intent);

		} else if (url.startsWith("rate:")) {
			final String app_package = getPackageName(); //requesting app package name from Context or Activity object
			try {
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + app_package)));
			} catch (ActivityNotFoundException anfe) {
				startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + app_package)));
			}

			// sharing content from your webview to external apps :: href="share:URL" and remember to place the URL you want to share after share:___
		} else if (url.startsWith("share:")) {
			Intent intent = new Intent(Intent.ACTION_SEND);
			intent.setType("text/plain");
			intent.putExtra(Intent.EXTRA_SUBJECT, view.getTitle());
			intent.putExtra(Intent.EXTRA_TEXT, view.getTitle() + "\nVisit: " + (Uri.parse(url).toString()).replace("share:", ""));
			startActivity(Intent.createChooser(intent, getString(R.string.share_w_friends)));

			// use this in a hyperlink to exit your app :: href="exit:android"
		} else if (url.startsWith("exit:")) {
			aswm_exit();

			// getting location for offline files
		} else if (url.startsWith("offloc:")) {
			String offloc = ASWV_URL + "?loc=" + get_location();
			aswm_view(offloc, false, asw_error_counter);
			Log.d("OFFLINE LOC REQ", offloc);

			// creating firebase notification for offline files
		} else if (url.startsWith("fcm:")) {
			String fcm = ASWV_URL + "?fcm=";
			aswm_view(fcm, false, asw_error_counter);
			Log.d("OFFLINE_FCM_TOKEN", fcm);


			// opening external URLs in android default web browser
		} else if (CONFIG_EXTURL && !aswm_host(url).equals(ASWV_HOST) && !CONFIG_EXC_LIST.contains(aswm_host(url))) {
			aswm_view(url, false, asw_error_counter);

			// else return false for no special action
		} else {
			a = false;
		}
		return a;
	}

	//method to get the right URL to use in the intent
	public String getFacebookPageURL(Context context, String url) {
		PackageManager packageManager = context.getPackageManager();
		try {
			int versionCode = packageManager.getPackageInfo("com.facebook.katana", 0).versionCode;
			if (versionCode >= 3002850) { //newer versions of fb app
				return "fb://facewebmodal/f?href=" + url;
			} else { //older versions of fb app
				return "fb://page/" + url;
			}
		} catch (PackageManager.NameNotFoundException e) {
			return url; //normal web url
		}
	}

	//Getting host name
	public static String aswm_host(String url) {
		if (url == null || url.length() == 0) {
			return "";
		}
		int dslash = url.indexOf("//");
		if (dslash == -1) {
			dslash = 0;
		} else {
			dslash += 2;
		}
		int end = url.indexOf('/', dslash);
		end = end >= 0 ? end : url.length();
		int port = url.indexOf(':', dslash);
		end = (port > 0 && port < end) ? port : end;
		Log.w("URL Host: ", url.substring(dslash, end));
		return url.substring(dslash, end);
	}

	//Reloading current page
	public void pull_fresh() {
		aswm_view((!CURR_URL.equals("") ? CURR_URL : ASWV_URL), false, asw_error_counter);
	}

	//Getting device basic information
	public void get_info() {
		if (true_online) {

			CookieManager cookieManager = CookieManager.getInstance();
			cookieManager.setAcceptCookie(true);
			cookieManager.setCookie(ASWV_URL, "DEVICE=android");
			DeviceDetails dv = new DeviceDetails();
			cookieManager.setCookie(ASWV_URL, "DEVICE_INFO=" + dv.pull());
			cookieManager.setCookie(ASWV_URL, "DEV_API=" + Build.VERSION.SDK_INT);
			cookieManager.setCookie(ASWV_URL, "APP_ID=" + BuildConfig.APPLICATION_ID);
			cookieManager.setCookie(ASWV_URL, "APP_VER=" + BuildConfig.VERSION_CODE + "/" + BuildConfig.VERSION_NAME);
			Log.d("COOKIES: ", cookieManager.getCookie(ASWV_URL));
		}
	}

	//Checking permission for storage and camera for writing and uploading images
	public void get_file() {
		String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA};

		//Checking for storage permission to write images for upload
		if (CONFIG_FUPLOAD && CONFIG_CAMUPLOAD && !check_permission(2) && !check_permission(3)) {
			ActivityCompat.requestPermissions(MainActivity.this, perms, file_perm);

			//Checking for WRITE_EXTERNAL_STORAGE permission
		} else if (CONFIG_FUPLOAD && !check_permission(2)) {
			ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, file_perm);

			//Checking for CAMERA permissions
		} else if (CONFIG_CAMUPLOAD && !check_permission(3)) {
			ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CAMERA}, file_perm);
		}
	}

	//Using cookies to update user locations
	public String get_location() {
		String newloc = "0,0";
		//Checking for location permissions
		if (CONFIG_LOCATION && (Build.VERSION.SDK_INT < 23 || check_permission(1))) {
			GPSTrack gps;
			gps = new GPSTrack(MainActivity.this);
			double latitude = gps.getLatitude();
			double longitude = gps.getLongitude();
			if (gps.canGetLocation()) {
				if (latitude != 0 || longitude != 0) {
					if (true_online) {
						CookieManager cookieManager = CookieManager.getInstance();
						cookieManager.setAcceptCookie(true);
						cookieManager.setCookie(ASWV_URL, "lat=" + latitude);
						cookieManager.setCookie(ASWV_URL, "long=" + longitude);
						cookieManager.setCookie(ASWV_URL, "LATLANG=" + latitude + "x" + longitude);
					}
					//Log.w("New Updated Location:", latitude + "," + longitude);  //enable to test dummy latitude and longitude
					newloc = latitude + "," + longitude;
				} else {
					Log.w("New Updated Location:", "NULL");
				}
			} else {

				Log.w("New Updated Location:", "FAIL");
			}
		}
		return newloc;
	}

	// get cookie value
	public String get_cookies(String cookie) {
		String value = "";
		CookieManager cookieManager = CookieManager.getInstance();
		String cookies = cookieManager.getCookie(ASWV_URL);
		String[] temp = cookies.split(";");
		for (String ar1 : temp) {
			if (ar1.contains(cookie)) {
				String[] temp1 = ar1.split("=");
				value = temp1[1];
				break;
			}
		}
		return value;
	}

	private static final Pattern urlPattern = Pattern.compile(
			"(?:^|[\\W])((ht|f)tp(s?):\\/\\/|www\\.)" + "(([\\w\\-]+\\.){1,}?([\\w\\-.~]+\\/?)*" + "[\\p{Alnum}.,%_=?&#\\-+()\\[\\]\\*$~@!:/{};']*)", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);

	@SuppressLint("ResourceAsColor")
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		final SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
		searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
		searchView.setQueryHint(getString(R.string.search_hint));
		searchView.setIconified(true);
		searchView.setIconifiedByDefault(true);
		searchView.clearFocus();

		searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextSubmit(String query) {
				searchView.clearFocus();
				aswm_view(ASWV_SEARCH + query, false, asw_error_counter);
				searchView.setQuery(query, false);
				return false;
			}

			@Override
			public boolean onQueryTextChange(String query) {
				return false;
			}
		});
		//searchView.setQuery(asw_view.getUrl(),false);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_exit) {
			aswm_exit();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public boolean onNavigationItemSelected(MenuItem item) {
		return false;
	}

	public static int aswm_fcm_id() {
		//Date now = new Date();
		//Integer.parseInt(new SimpleDateFormat("ddHHmmss",  Locale.US).format(now));
		return 1;
	}


	//Checking if particular permission is given or not
	public boolean check_permission(int permission) {
		switch (permission) {
			case 1:
				return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;

			case 2:
				return ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;

			case 3:
				return ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;

		}
		return false;
	}

	//Creating image file for upload
	private File create_image() throws IOException {
		@SuppressLint("SimpleDateFormat")
		String file_name = new SimpleDateFormat("yyyy_mm_ss").format(new Date());
		String new_name = "file_" + file_name + "_";
		File sd_directory = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
		return File.createTempFile(new_name, ".jpg", sd_directory);
	}

	//Creating video file for upload
	private File create_video() throws IOException {
		@SuppressLint("SimpleDateFormat")
		String file_name = new SimpleDateFormat("yyyy_mm_ss").format(new Date());
		String new_name = "file_" + file_name + "_";
		File sd_directory = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
		return File.createTempFile(new_name, ".3gp", sd_directory);
	}

	//Launching app rating dialoge [developed by github.com/hotchemi]
	public void get_rating() {
		if (DetectConnection.isInternetAvailable(MainActivity.this)) {
			AppRate.with(this)
					.setStoreType(StoreType.GOOGLEPLAY)     //default is Google Play, other option is Amazon App Store
					.setInstallDays(WebViewConfig.ASWR_DAYS)
					.setLaunchTimes(WebViewConfig.ASWR_TIMES)
					.setRemindInterval(WebViewConfig.ASWR_INTERVAL)
					.setTitle(R.string.rate_dialog_title)
					.setMessage(R.string.rate_dialog_message)
					.setTextLater(R.string.rate_dialog_cancel)
					.setTextNever(R.string.rate_dialog_no)
					.setTextRateNow(R.string.rate_dialog_ok)
					.monitor();
			AppRate.showRateDialogIfMeetsConditions(this);
		}
		//for more customizations, look for AppRate and DialogManager
	}

	//Creating custom notifications with IDs

	//Printing pages
	private void print_page(WebView view, String print_name, boolean manual) {
		PrintManager printManager = (PrintManager) getSystemService(Context.PRINT_SERVICE);
		PrintDocumentAdapter printAdapter = view.createPrintDocumentAdapter(print_name);
		PrintAttributes.Builder builder = new PrintAttributes.Builder();
		builder.setMediaSize(PrintAttributes.MediaSize.ISO_A5);
		PrintJob printJob = printManager.print(print_name, printAdapter, builder.build());

		if (printJob.isCompleted()) {
			Toast.makeText(getApplicationContext(), R.string.print_complete, Toast.LENGTH_LONG).show();
		} else if (printJob.isFailed()) {
			Toast.makeText(getApplicationContext(), R.string.print_failed, Toast.LENGTH_LONG).show();
		}
	}

	//Checking if users allowed the requested permissions or not
	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		if (requestCode == 1) {
			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				get_location();
			}
		}
	}

	//Action on back key tap/click
	@Override
	public boolean onKeyDown(int keyCode, @NonNull KeyEvent event) {
		if (event.getAction() == KeyEvent.ACTION_DOWN) {
			if (keyCode == KeyEvent.KEYCODE_BACK) {
				if (mainWebView.canGoBack()) {
					if (!mainWebView.getUrl().startsWith("https://app.afo.fit")) {
						mainWebView.goBack();
					} else {
						ask_exit();
					}
				} else if (mWebviewPop != null && mWebviewPop.canGoBack()) {
					mWebviewPop.goBack();
				} else {
					if (!mainWebView.getUrl().startsWith("https://app.afo.fit")) {
						ask_exit();
					}
					ask_exit();
				}
				return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}

	public void aswm_exit() {
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	// Creating exit dialogue
	public void ask_exit() {
		AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

		builder.setTitle(getString(R.string.exit_title));
		builder.setMessage(getString(R.string.exit_subtitle));
		builder.setCancelable(true);

		// Action if user selects 'yes'
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialogInterface, int i) {
				finish();
			}
		});

		// Actions if user selects 'no'
		builder.setNegativeButton("No", (dialogInterface, i) -> {
		});

		// Create the alert dialog using alert dialog builder
		AlertDialog dialog = builder.create();

		// Finally, display the dialog when user press back button
		dialog.show();
	}

	@Override
	protected void onStart() {
		super.onStart();

		FirebaseUser currentUser = mAuth.getCurrentUser();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		mainWebView.saveState(outState);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		mainWebView.restoreState(savedInstanceState);
	}

	private WebView getMainWebView() {

		WebView mainWebView = findViewById(R.id.webview);
		// mainWebView.setBackgroundColor(R.color.colorPrimaryDarker);


		//Webview settings; defaults are customized for best performance
		WebSettings webSettings = mainWebView.getSettings();

		// setting custom user agent
		if (OVERRIDE_USER_AGENT || POSTFIX_USER_AGENT) {
			String userAgent = webSettings.getUserAgentString();
			if (OVERRIDE_USER_AGENT) {
				userAgent = CUSTOM_USER_AGENT;
			}
			if (POSTFIX_USER_AGENT) {
				userAgent = userAgent + " " + USER_AGENT_POSTFIX;
			}
			webSettings.setUserAgentString(userAgent);
		}

		if (!CONFIG_OFFLINE) {
			webSettings.setJavaScriptEnabled(CONFIG_JSCRIPT);
		}
		webSettings.setSaveFormData(CONFIG_SFORM);
		webSettings.setSupportZoom(CONFIG_ZOOM);
		webSettings.setGeolocationEnabled(CONFIG_LOCATION);
		webSettings.setAllowFileAccess(true);
		webSettings.setAllowFileAccessFromFileURLs(true);
		webSettings.setAllowUniversalAccessFromFileURLs(true);
		webSettings.setUseWideViewPort(true);
		webSettings.setDomStorageEnabled(true);

		if (!CONFIG_CP) {
			mainWebView.setOnLongClickListener(new View.OnLongClickListener() {
				@Override
				public boolean onLongClick(View v) {
					return true;
				}
			});
		}
		mainWebView.setHapticFeedbackEnabled(false);
		mainWebView.addJavascriptInterface(new WebAppInterface(mContext), "appInterface");

		// download listener
		mainWebView.setDownloadListener(new DownloadListener() {
			@Override
			public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimeType, long contentLength) {

				if (!check_permission(2)) {
					ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, file_perm);
				} else {
					DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));

					request.setMimeType(mimeType);
					String cookies = CookieManager.getInstance().getCookie(url);
					request.addRequestHeader("cookie", cookies);
					request.addRequestHeader("User-Agent", userAgent);
					request.setDescription(getString(R.string.dl_downloading));
					request.setTitle(URLUtil.guessFileName(url, contentDisposition, mimeType));
					request.allowScanningByMediaScanner();
					request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
					request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, URLUtil.guessFileName(url, contentDisposition, mimeType));
					DownloadManager dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
					assert dm != null;
					dm.enqueue(request);
					Toast.makeText(getApplicationContext(), getString(R.string.dl_downloading2), Toast.LENGTH_LONG).show();
				}
			}
		});

		getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
		getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
		webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
		mainWebView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
		mainWebView.setVerticalScrollBarEnabled(false);
		mainWebView.setWebViewClient(new Callback());
		mainWebView.setWebChromeClient(new CustomerChromeClient());

		mainWebView.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View view, MotionEvent motionEvent) {
				WebView.HitTestResult hr = ((WebView) view).getHitTestResult();
				if (hr != null && lastURL != null) {
					//previousURL = lastURL;
					Log.i("DebugDebug", "getExtra = " + hr.getExtra() + "\t\t Type = " + hr.getType());
				}
				return false;
			}
		});

		return mainWebView;
	}


	private void readIntents() {
		Intent read_int = getIntent();
		Log.d("INTENT", read_int.toUri(0));
		String uri = read_int.getStringExtra("uri");
		String share = read_int.getStringExtra("s_uri");
		String share_img = read_int.getStringExtra("s_img");

		if (share != null) {
			//Processing shared content
			Log.d("SHARE INTENT", share);
			Matcher matcher = urlPattern.matcher(share);
			String urlStr = "";
			if (matcher.find()) {
				urlStr = matcher.group();
				if (urlStr.startsWith("(") && urlStr.endsWith(")")) {
					urlStr = urlStr.substring(1, urlStr.length() - 1);
				}
			}
			String red_url = ASWV_SHARE_URL + "?text=" + share + "&link=" + urlStr + "&image_url=";
			//Toast.makeText(MainActivity.this, "SHARE: "+red_url+"\nLINK: "+urlStr, Toast.LENGTH_LONG).show();
			aswm_view(red_url, false, asw_error_counter);

		} else if (share_img != null) {
			//Processing shared content
			Log.d("SHARE INTENT", share_img);
			Toast.makeText(MainActivity.this, share_img, Toast.LENGTH_LONG).show();
			aswm_view(ASWV_URL, false, asw_error_counter);

		} else if (uri != null) {
			//Opening notification
			Log.d("NOTIFICATION INTENT", uri);
			aswm_view(uri, false, asw_error_counter);

		} else {
			//Rendering the default URL
			Log.d("MAIN INTENT", ASWV_URL);
			final String renderURL = buildURL();
			aswm_view(renderURL, false, asw_error_counter);

		}
	}

	private String buildURL() {

		String url = ASWV_URL + "&device_id=" + android_id;


		if (authCode != null) {
			url = url + "&code=" + authCode;
		}

		if (eventId != null) {
			url = url + "&eventId=" + eventId;
		}

		if (referredBy != null) {
			url = url + "&referredBy=" + referredBy;
		}

		if (notificationAction != null) {
			url = url + "&notifactionAction=" + notificationAction;
		}


		return url;
	}


	public class WebAppInterface {

		WebAppInterface(Context c) {
			mContext = c;
		}

		@JavascriptInterface
		public void shareScreenShot(String text, boolean isSquare) {

			Bitmap bitmap = null;
			if (isSquare) {
				bitmap = Bitmap.createBitmap(mainWebView.getWidth(),
						mainWebView.getWidth(), Bitmap.Config.ARGB_8888);
			} else {
				bitmap = Bitmap.createBitmap(mainWebView.getWidth(),
						mainWebView.getHeight(), Bitmap.Config.ARGB_8888);
			}
			Canvas canvas = new Canvas(bitmap);
			mainWebView.draw(canvas);
			String pathofBmp =
					MediaStore.Images.Media.insertImage(getContentResolver(),
							bitmap, "afo" + Math.random(), null);
			Uri uri = Uri.parse(pathofBmp);
			Intent shareIntent = new Intent(Intent.ACTION_SEND);
			Toast.makeText(mContext, "Image Downloaded", Toast.LENGTH_SHORT).show();

			shareIntent.setType("image/*");
			shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
			shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Star App");

			startActivity(Intent.createChooser(shareIntent, "Select App"));
		}

		@JavascriptInterface
		public void share(String text, String imageUrl) {

			Intent shareIntent = new Intent(Intent.ACTION_SEND);
			if (imageUrl != null) {
				Bitmap bitmap = null;
				Uri uri = null;
				try {
					URL url = new URL(imageUrl);
					HttpURLConnection connection = null;
					connection = (HttpURLConnection) url.openConnection();
					connection.setDoInput(true);
					connection.connect();
					InputStream input = connection.getInputStream();
					bitmap = BitmapFactory.decodeStream(input);
					String pathofBmp =
							MediaStore.Images.Media.insertImage(getContentResolver(),
									bitmap, "afo", null);
					uri = Uri.parse(pathofBmp);
				} catch (IOException e) {
					System.out.println("");
				}

				shareIntent.setType("image/*");
				shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
				Toast.makeText(mContext, "Image Downloaded", Toast.LENGTH_SHORT).show();
			} else {
				shareIntent.setType("text/plain");
			}
			shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Star App");
			shareIntent.putExtra(Intent.EXTRA_TEXT, text);


			startActivity(Intent.createChooser(shareIntent, "Select App"));
		}

		@JavascriptInterface
		public void updateUserId(String userId) {
			signIn();
			prefManager.setUserID(userId);
			FirebaseMessaging.getInstance().getToken()
					.addOnCompleteListener(new OnCompleteListener<String>() {
						@Override
						public void onComplete(@NonNull Task<String> task) {
							if (!task.isSuccessful()) {
								Log.w(TAG, "Fetching FCM registration token failed", task.getException());
								return;
							}

							// Get new FCM registration token
							firestoreDB.updateFCMToken(userId, task.getResult());
						}
					});

		}

		@JavascriptInterface
		public void download(String url) {
			if (!check_permission(2)) {
				ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, file_perm);
			} else {
				String[] arr = url.split("/");
				String filename = arr[arr.length - 1];
				String downloadUrlOfImage = url;
				File direct =
						new File(Environment
								.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
								.getAbsolutePath() + "/" + "afo" + "/");


				if (!direct.exists()) {
					direct.mkdir();

				}

				DownloadManager dm = (DownloadManager) mContext.getSystemService(Context.DOWNLOAD_SERVICE);
				Uri downloadUri = Uri.parse(downloadUrlOfImage);
				DownloadManager.Request request = new DownloadManager.Request(downloadUri);
				request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
						.setAllowedOverRoaming(false)
						.setTitle(filename)
						.setMimeType("image/jpeg")
						.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
						.setDestinationInExternalPublicDir(Environment.DIRECTORY_PICTURES,
								File.separator + "afo" + File.separator + filename);

				Toast.makeText(mContext, "Image Downloaded", Toast.LENGTH_SHORT).show();
				dm.enqueue(request);
			}
		}
	}

	private void signIn() {
		mAuth.signInAnonymously()
				.addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
					@Override
					public void onComplete(@NonNull Task<AuthResult> task) {
						if (task.isSuccessful()) {
							// Sign in success, update UI with the signed-in user's information
							Log.d(TAG, "signInAnonymously:success");
							FirebaseUser user = mAuth.getCurrentUser();

						}
					}
				});
	}


	class CustomerChromeClient extends WebChromeClient {

		@Override
		public boolean onCreateWindow(WebView view, boolean isDialog,
									  boolean isUserGesture, Message resultMsg) {
			mWebviewPop = new WebView(mContext);
			mWebviewPop.setVerticalScrollBarEnabled(false);
			mWebviewPop.setHorizontalScrollBarEnabled(false);
			mWebviewPop.setWebViewClient(new Callback());
			mWebviewPop.setWebChromeClient(new CustomerChromeClient());
			mWebviewPop.getSettings().setJavaScriptEnabled(true);
			mWebviewPop.getSettings().setSavePassword(true);
			mWebviewPop.getSettings().setSaveFormData(true);
			mWebviewPop.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

			// create an AlertDialog.Builder
			//the below did not give me .dismiss() method . See : https://stackoverflow.com/questions/14853325/how-to-dismiss-alertdialog-in-android

			AlertDialog builder;
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                builder = new AlertDialog.Builder(MainActivity.this, android.R.style.Theme_Material_Dialog_Alert);
//            } else {
//                builder = new AlertDialog(MainActivity.this);
//            }

			// set the WebView as the AlertDialog.Builder’s view

			builder = new AlertDialog.Builder(MainActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT).create();


			builder.setTitle("");
			builder.setView(mWebviewPop);

			builder.setButton("Close", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int id) {
					mWebviewPop.destroy();
					dialog.dismiss();


				}
			});

			builder.show();
			builder.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);

			CookieManager cookieManager = CookieManager.getInstance();
			cookieManager.setAcceptCookie(true);
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
				cookieManager.setAcceptThirdPartyCookies(mWebviewPop, true);
			}


			WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
			transport.setWebView(mWebviewPop);
			resultMsg.sendToTarget();

			return true;
		}

		// handling input[type="file"]
		public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
			if (check_permission(2) && check_permission(3)) {
				if (CONFIG_FUPLOAD) {
					asw_file_path = filePathCallback;
					Intent takePictureIntent = null;
					Intent takeVideoIntent = null;
					if (CONFIG_CAMUPLOAD) {
						boolean includeVideo = false;
						boolean includePhoto = true;
						System.out.println("FileParam Accepts Types :"+ fileChooserParams.getAcceptTypes());
						// Check the accept parameter to determine which intent(s) to include.
						paramCheck:
						for (String acceptTypes : fileChooserParams.getAcceptTypes()) {
							// Although it's an array, it still seems to be the whole value.
							// Split it out into chunks so that we can detect multiple values.
							String[] splitTypes = acceptTypes.split(", ?+");
							for (String acceptType : splitTypes) {
								switch (acceptType) {
									case "*/*":
										includePhoto = true;
										includeVideo = true;
										break paramCheck;
									case "image/*":
										includePhoto = true;
										break;
									case "video/*":
										includeVideo = false;
										break;
								}
							}
						}

						// If no `accept` parameter was specified, allow both photo and video.
						if (fileChooserParams.getAcceptTypes().length == 0) {
							includePhoto = true;
							includeVideo = true;
						}

						ImagePicker.Companion.with(MainActivity.this)
								.crop()
								.compress(1024)//Crop image(Optional), Check Customization for more option//Allow dimmed layer to have a circle inside
       //We have to define what image provider we want to use
								.maxResultSize(1080, 1080).start();
//
//						if (includePhoto) {
//							takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//							if (takePictureIntent.resolveActivity(MainActivity.this.getPackageManager()) != null) {
//								File photoFile = null;
//								try {
//									photoFile = create_image();
//									takePictureIntent.putExtra("PhotoPath", asw_pcam_message);
//								} catch (IOException ex) {
//									Log.e(TAG, "Image file creation failed", ex);
//								}
//								if (photoFile != null) {
//									asw_pcam_message = "file:" + photoFile.getAbsolutePath();
//									takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
//								} else {
//									takePictureIntent = null;
//								}
//							}
//						}

//						if (includeVideo) {
//							takeVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
//							if (takeVideoIntent.resolveActivity(MainActivity.this.getPackageManager()) != null) {
//								File videoFile = null;
//								try {
//									videoFile = create_video();
//								} catch (IOException ex) {
//									Log.e(TAG, "Video file creation failed", ex);
//								}
//								if (videoFile != null) {
//									asw_vcam_message = "file:" + videoFile.getAbsolutePath();
//									takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(videoFile));
//								} else {
//									takeVideoIntent = null;
//								}
//							}
//						}
					}

//					Intent contentSelectionIntent = new Intent(Intent.ACTION_GET_CONTENT);
//					if (!CONFIG_ONLYCAM) {
//						contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE);
//						contentSelectionIntent.setType(ASWV_F_TYPE);
//						if (CONFIG_MULFILE) {
//							final Intent intent = contentSelectionIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
//						}
//					}
//					Intent[] intentArray;
//					if (takePictureIntent != null && takeVideoIntent != null) {
//						intentArray = new Intent[]{takePictureIntent, takeVideoIntent};
//					} else if (takePictureIntent != null) {
//						Intent galleryIntent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//						intentArray = new Intent[]{takePictureIntent,galleryIntent};
//					} else if (takeVideoIntent != null) {
//						intentArray = new Intent[]{takeVideoIntent};
//					} else {
//						intentArray = new Intent[0];
//					}
//
//					Intent chooserIntent = new Intent(Intent.ACTION_CHOOSER);
//					chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent);
//					chooserIntent.putExtra(Intent.EXTRA_TITLE, getString(R.string.fl_chooser));
//					chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
//					startActivityForResult(chooserIntent, asw_file_req);
				}
				return true;
			} else{
			get_file();
			return false;
		}
	}


	public Bitmap getBitmapfromUrl(String imageUrl) {
		try {
			URL url = new URL(imageUrl);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoInput(true);
			connection.connect();
			InputStream input = connection.getInputStream();
			return BitmapFactory.decodeStream(input);

		} catch (Exception e) {
			Log.e("awesome", "Error in getting image: " + e.getLocalizedMessage());
			return null;
		}
	}


	@Override
	public void onCloseWindow(WebView window) {

		//Toast.makeText(mContext,"onCloseWindow called",Toast.LENGTH_SHORT).show();
		try {
			mWebviewPop.destroy();
		} catch (Exception e) {

		}

		try {
			builder.dismiss();

		} catch (Exception e) {

		}
	}

}


}
