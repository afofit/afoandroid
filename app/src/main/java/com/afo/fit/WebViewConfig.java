package com.afo.fit;

/*
 * Android Smart WebView is an Open Source Project available on GitHub (https://github.com/mgks/Android-SmartWebView).
 * Initially developed by Ghazi Khan (https://github.com/mgks), under MIT Open Source License.
 * This program is free to use for private and commercial purposes.
 * Enhance Smart WebView with plugins - https://voinsource.github.io/#plugins (Google Login, Background Services, Vision API, Advance Notifications, PQL etc).
 * Please mention project source or credit developers in your Application's License(s) Wiki.
 * Giving right credit to developers encourages them to create better projects :)
 */

class WebViewConfig {

	/* -- PERMISSION VARIABLES -- */
	static boolean CONFIG_JSCRIPT       = true;         // enable JavaScript for webview
	static boolean CONFIG_FUPLOAD       = true;         // upload file from webview
	static boolean CONFIG_CAMUPLOAD     = true;         // enable upload from camera for photos
	static boolean CONFIG_ONLYCAM       = false;        // incase you want only camera files to upload
	static boolean CONFIG_MULFILE       = true;         // upload multiple files in webview
	static boolean CONFIG_LOCATION      = false;         // track GPS locations
	static boolean CONFIG_CP            = false;        // enable copy/paste within webview

	static boolean CONFIG_RATINGS       = false;         // show ratings dialog; auto configured ; edit method get_rating() for customizations

	static boolean CONFIG_PULLFRESH     = true;         // pull refresh current url
	static boolean CONFIG_PBAR          = false;         // show progress bar in app
	static boolean CONFIG_ZOOM          = false;        // zoom control for webpages view
	static boolean CONFIG_SFORM         = true;        // save form cache and auto-fill information
	static boolean CONFIG_OFFLINE       = false;        // whether the loading webpages are offline or online
	static boolean CONFIG_EXTURL        = true;         // open external url with default browser instead of app webview

	static boolean CONFIG_TAB           = true;         // instead of default browser, open external URLs in chrome tab
	static boolean CONFIG_ADMOB         = false;         // to load admob or not

	static boolean CONFIG_EXITDIAL	  = false;         // confirm to exit app on back press

	/* -- SECURITY VARIABLES -- */
	static boolean CONFIG_CERT_VERIFICATION    = true;         // verify whether HTTPS port needs certificate verification


	/* -- CONFIG VARIABLES -- */
	// layout selection
	static int ASWV_LAYOUT            = 0;            // default=0; for clear fullscreen layout, and =1 for drawer layout

	// URL configs
	static boolean IS_TEST = false;
	static String CONFIG_URL            = "https://app.afo.fit?device_type=android";  // complete URL of your website or offline webpage "file:///android_asset/offline.html";
	static String CONFIG_URL_TEST           = "https://app.afo.fit/version-test?device_type=android";  // complete URL of your website or offline webpage "file:///android_asset/offline.html";

	static String ASWV_SEARCH         = "https://www.google.com/search?q=";         // search query will start by the end of the present string
	static String ASWV_SHARE_URL      = CONFIG_URL + "?share=";                       // URL where you process external content shared with the app
	// domains allowed to be opened inside webview
	static String CONFIG_EXC_LIST = "app.afo.fit";       //separate domains with a comma (,)
	static String GOOGLE_REDIRECTION_PREFIX = "https://accounts.google.com";
	static String FACEBOOK_REDIRECTION_PREFIX = "https://www.facebook.com/";

	// custom user agent defaults
	static boolean POSTFIX_USER_AGENT       = true;         // set to true to append USER_AGENT_POSTFIX to user agent
	static boolean OVERRIDE_USER_AGENT      = true;        // set to true to use USER_AGENT instead of default one
	static String USER_AGENT_POSTFIX        = "AfoAndroid"; // useful for identifying traffic, e.g. in Google Analytics
	static String CUSTOM_USER_AGENT         = "Mozilla/5.0 (Linux; Android 6.0;One Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Mobile Safari/537.36";    // custom user-agent

	// to upload any file type using "*/*"; check file type references for more
	static String ASWV_F_TYPE         = "*/*";

	// admob config
	static String ASWV_ADMOB          = "pub-1566476371455791";		// your unique publishers ID


	/* -- RATING SYSTEM VARIABLES -- */
	static int ASWR_DAYS      = 3;            // after how many days of usage would you like to show the dialog
	static int ASWR_TIMES     = 10;           // overall request launch times being ignored
	static int ASWR_INTERVAL  = 2;            // reminding users to rate after days interval

}
