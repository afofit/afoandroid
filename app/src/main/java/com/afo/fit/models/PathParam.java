package com.afo.fit.models;

import android.os.Parcel;
import android.os.Parcelable;

public class PathParam implements Parcelable {

	private String eventId;
	private String code;
	private String referredBy;
	private boolean isFirstTime = false;
	private String notificationAction;

	public PathParam(String eventId, String code, String referredBy, boolean isFirstTime,String notificationAction) {
		this.eventId = eventId;
		this.code = code;
		this.referredBy = referredBy;
		this.isFirstTime = isFirstTime;
		this.notificationAction = notificationAction;
	}

	public PathParam(Parcel parcel) {
		eventId = parcel.readString();
		code = parcel.readString();
		referredBy = parcel.readString();
		isFirstTime = (Boolean) parcel.readValue(null);
		notificationAction = parcel.readString();
	}

	public boolean isFirstTime() {
		return isFirstTime;
	}

	public void setFirstTime(boolean firstTime) {
		isFirstTime = firstTime;
	}

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getReferredBy() {
		return referredBy;
	}

	public void setReferredBy(String referredBy) {
		this.referredBy = referredBy;
	}

	public String getNotificationAction() {
		return notificationAction;
	}

	public void setNotificationAction(String notificationAction) {
		this.notificationAction = notificationAction;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(eventId);
		dest.writeString(code);
		dest.writeString(referredBy);
		dest.writeValue(isFirstTime);
		dest.writeValue(notificationAction);
	}

	//used when un-parceling our parcel (creating the object)
	public static final Parcelable.Creator<PathParam> CREATOR = new Parcelable.Creator<PathParam>(){

		@Override
		public PathParam createFromParcel(Parcel parcel) {
			return new PathParam(parcel);
		}

		@Override
		public PathParam[] newArray(int size) {
			return new PathParam[0];
		}
	};
}
